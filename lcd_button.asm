; DEFINITIONS
.include "m8515def.inc"
.def temp = r16 ; temporary register
.def computerAns = r17
.def sem =r18
.def Wait2 = r21
.def Wait3 = r19
.def value1 = r20
.def flagButton = r22
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25

; RESET and INTERRUPT VECTORS
.org $00
rjmp MAIN
.org $01
rjmp ext_int0
.org $02
rjmp ext_int1

; CODE SEGMENT
MAIN:

INIT_LED:
	ser sem 				
	out DDRC,sem 			
	ldi	temp,low(RAMEND) 	
	out	SPL,temp	 		
	ldi	temp,high(RAMEND)
	out	SPH,temp

INIT_STACK:
	ldi temp, low(RAMEND)
	ldi temp, high(RAMEND)
	out SPH, temp

    rcall CLEAR_LCD

INIT_INTERRUPT:
	ldi temp,0b00001010
	out MCUCR,temp
	ldi temp,0b11000000
	out GICR,temp
	sei

rcall COMMAND

EXIT:
	rjmp EXIT

ext_int0:
	ldi flagButton,1
	reti

ext_int1:
    ldi flagButton,2
    reti

COMMAND:
    ldi computerAns,1
    cpi flagButton,0
    brne LOAD_COMPUTE

    ldi computerAns,2
    cpi flagButton,0
    brne LOAD_COMPUTE

    rjmp COMMAND

LOAD_COMPUTE:
    cpi flagButton,1 ; jika pick LEFT, load string kiri
    breq LOAD_LEFT
    
    cpi flagButton,2 ; jika RIGHT, load string kanan
    breq LOAD_RIGHT

COMPARE:
	rcall CEK_KOMP_ANS
    rcall DOWN
    cp computerAns,flagButton
	rcall VALUE_SATU
    breq LOAD_WIN

    cp computerAns,flagButton
	rcall VALUE_DUA
    brne LOAD_LOSE

VALUE_SATU:
	ldi sem,0b00001111
	out PORTC,sem ; Update LEDS
	ret


VALUE_DUA:
	ldi sem,0b11110000
	out PORTC,sem ; Update LEDS
	ret

CEK_KOMP_ANS:
    cpi computerAns,1 ; jika pick LEFT, load string kiri
    breq LOAD_LEFT_1
    
    cpi computerAns,2 ; jika RIGHT, load string kanan
    breq LOAD_RIGHT_1

LOAD_LEFT_1:
	ldi ZH,high(2*LEFT1) ; Load high part of byte address into ZH
	ldi ZL,low(2*LEFT1) ; Load low part of byte address into ZL
	rjmp LOADBYTE

LOAD_RIGHT_1:
    ldi ZH,high(2*RIGHT1) ; Load high part of byte address into ZH
	ldi ZL,low(2*RIGHT1) ; Load low part of byte address into ZL
	rjmp LOADBYTE
	
	

LOAD_WIN:
    ldi ZH,high(2*WIN)
    ldi ZL,low(2*WIN)
    rjmp LOADBYTE

LOAD_LOSE:
    ldi ZH,high(2*LOSE)
    ldi ZL,low(2*LOSE)
    rjmp LOADBYTE

GO_RIGHT:
	cbi PORTA, 1   ; RS = 0
	cbi PORTA, 2   ; R/W = 0
	ldi PB, 0x08	; set cursor to C0
	out PORTB, PB  ; DDRAM = 0xC0
	sbi PORTA, 0   ; EN = 1
	cbi PORTA, 0   ; EN = 0
	ret

DOWN:
	cbi PORTA, 1   ; RS = 0
	cbi PORTA, 2   ; R/W = 0
	ldi PB, 0xC0   ; set cursor to C0
	out PORTB, PB  ; DDRAM = 0xC0
	sbi PORTA, 0   ; EN = 1
	cbi PORTA, 0   ; EN = 0
	ret



LOAD_LEFT:
    ldi ZH,high(2*LEFT) ; Load high part of byte address into ZH
	ldi ZL,low(2*LEFT) ; Load low part of byte address into ZL
    rcall INIT_LCD_MAIN
    rcall COMPARE
    ldi flagButton, 0

	rjmp COMMAND


LOAD_RIGHT:
    ldi ZH,high(2*RIGHT) ; Load high part of byte address into ZH
	ldi ZL,low(2*RIGHT) ; Load low part of byte address into ZL
    rcall INIT_LCD_MAIN
    rcall COMPARE
    ldi flagButton, 0

	rjmp COMMAND

INIT_LCD_MAIN:
    rcall INIT_LCD
	ser temp
	out DDRA,temp ; Set port A as output (LCD)

LOADBYTE:
    lpm
	tst r0 ; Check if we've reached the end of the message
	breq END_LCD
	mov A, r0 ; Put the character onto Port B
    rcall WRITE_TEXT
	adiw ZL,1 ; Increase Z registers
	rjmp LOADBYTE


	
END_LCD:
	ret

INIT_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,0x38 ; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	cbi PORTA,1 ; CLR RS
	ldi PB,$0E ; MOV DATA,0x0E --> disp ON, cursor ON, blink OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	rcall CLEAR_LCD ; CLEAR LCD
	cbi PORTA,1 ; CLR RS
	ldi PB,$06 ; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

CLEAR_LCD:
	cbi PORTA,1 ; CLR RS
	ldi PB,$01 ; MOV DATA,0x01
	out PORTB,PB
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

WRITE_TEXT:
	sbi PORTA,1 ; SETB RS
	out PORTB,A
	sbi PORTA,0 ; SETB EN
	cbi PORTA,0 ; CLR EN
	ret

; DATA
LEFT:
    .db "LEFT",0

RIGHT:
    .db "RIGHT",0

RIGHT1:
	.db "-RIGHT",0

LEFT1:
	.db "-LEFT",0

WIN:
    .db "YOU WIN",0

LOSE:
    .db "YOU LOSE",0
